# Description:
#   Wisdom from the internet from Logan
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot wise - Returns a Richard Stallman fact.
#
# Author:
#   oflogan


wisdom = [
  'What light? It is the east, and my waifu is the sun.',
  '“The wisdom of the Patriarchs” said Master Foo, “was that they knew they were fools.”',
  'In every operating system there is a path to the Great Way, if only we can find it.',
  'When you are hungry, eat; when you are thirsty, drink; when you are tired, sleep.',
  'At that moment, the programmer achieved enlightenment.',
  'Three pounds of VAX!',
  'There is more Unix-nature in one line of shell script than there is in ten thousand lines of C.',
  'Never underestimate the bandwidth of a station wagon full of tapes hurtling down the highway.',
  'The only way of finding the limits of the possible is by going beyond them into the impossible.',
  'Never tell me the odds.',
  'A man chooses. A slave obeys.',
  'The right man in the wrong place can make all the difference in the world.',
  'I saw the angel in the marble and carved until I set him free.',
  'If nobody comes back from the future to stop you, then how bad of a decision can it really be?',
  'I appreciate you.',
  'Look up at the moon. Stuart smiles on us.',
  'The motherland calls!',
]

module.exports = (robot) ->
  robot.respond /\bwisdom\b/i, (msg) ->
    msg.send msg.random wisdom
